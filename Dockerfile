FROM python:3.7

WORKDIR /app
COPY . /app

RUN /usr/local/bin/python -m  pip install --upgrade pip && pip install -r requirements.txt
CMD  ["uwsgi", "--http", ":8080", "--wsgi-file", "main.py" , "--callable", "app"]